/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.rmartinezch.interfaces;

/**
 *
 * @author Ronald Martinez <rmartinezch@uni.edu.pe>
 */
public class Chicken extends Animal implements Edible {

    @Override
    public String sound() {
        return "Chicken: ki ki ri ki ki!";
    }

    @Override
    public String howToEat() {
        return "Chicken: fry it!!!";
    }

}
