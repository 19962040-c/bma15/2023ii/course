/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pe.edu.uni.rmartinezch.interfaces;

/**
 *
 * @author Ronald Martinez <rmartinezch@uni.edu.pe>
 */
public interface Edible {
    public abstract String howToEat();
}
