/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.rmartinezch.exceptions;

/**
 *
 * @author Ronald Martinez <rmartinezch@uni.edu.pe>
 */
public class App {
    
    public static void main(String[] args) {
        System.out.println("Exception !!!");
        int[] array = {10, 20, 30, 40, 50, 60};
        int i = 0;
        /*
        if (i < array.length){
            System.out.println(array[i]);
        } else {
            System.out.println("Fuera de los límites");
        }
         */
        try {
            // abrir la Base de datos
            System.out.println(array[i]);
//            int j = 10 / i;
            Function(i);
            // cerrar la base de datos
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Fuera de los límites");
            System.out.println(e);
        } catch (ArithmeticException e) {
            System.out.println("División entre cero");
            System.out.println(e);
        } catch(Exception e) {
            System.out.println("Default !!!");
            System.out.println(e);
        } finally {
            System.out.println("Cierra la Base datos");
        }
    }
    
    public static void Function(int i) throws ArithmeticException {
        int j = 10 / i;
    }
}
